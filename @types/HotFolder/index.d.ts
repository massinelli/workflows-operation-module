import { Dirent } from 'fs'

type WorkflowTarget = 'add'|'chg'|'del'
interface GetTreeOptions{
  filterFiles:RegExp,
  filterFolders:RegExp
}
interface FileObj{file: Dirent, path:string, contains?:FileObj[]}

declare global{
  interface HotFolder{
    /**
   * 
   * @param includeDir optional boolean, if true also returns subdirectories
   * @param includeSubdirsFiles optional boolean, if true recursively get list of all files in subdirectories
   * @param extensions optional array of extensions (without the dot, for example ['pdf','txt'])
   * 
   * returns the list of file names (if includeSubDirsFiles is true it also returns the relative path) in the hotfolder path.
   * if optional extesions array is given the lists will be filtered 
   */
    getFiles(includeDir:boolean,includeSubdirsFiles:boolean,extensions?:string[]):string[]
  
    /**
     * 
     * returns the directories and files tree in the hotfolder path in form of js object.
     */
    getTree(options?:GetTreeOptions):FileObj[]

    skipNext(targets:WorkflowTarget[]):HotFolder
    skipNext(targets:WorkflowTarget[],filesToSkip:string[]):HotFolder
    skipNext(targets:WorkflowTarget[],numberOfFilesToSkip:number):HotFolder
    /**
     * 
     * @param targets an array of targets that can be 'add', 'chg' or 'del'
     * @param filesToSkip optional array of file names strings
     * @param numberOfFilesToSkip optional number
     * 
     * Adds temporary exclusions to the hotfolder and/or skip a given number of file.
     * When an array of file names is given, the hotfolder exclude that file names from
     * given targets until that file names are detected for the first time.
     * When a number is given, the hotfolder skip that number of next detected files
     */
    skipNext(targets:WorkflowTarget[],arg1?:number|string[],arg2?:string[]):HotFolder
  }
}
