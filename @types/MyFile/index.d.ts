import { ObjectId } from "bson"
import { Db } from 'mongodb'
import { Stats } from 'fs'

declare global{
  interface ImSetting{setting:string,val?:string|number}
  interface ImOperator{operator:string,val?:string|number}
  interface MyFileStats{
    birthDate?:number,
    modifyDate?:number,
    size?:number
  }

  /**
   * Class representing a workflow process file
   */
  class MyFile{
    path:string;
    name:string;
    /** file extension (with the dot!!) */
    ext:string;
    size:number;
    insertDate:number;
    deleted:number;
    modifyDate:number;
    birthDate:number;
    /** stores file physical changes */
    history: {
      date:number,
      path:string,
      modifyDate:number,
      size:number
    }[];
    /** when a file is copied it stores the copies */
    copies:{uid:ObjectId,date:number,isTheOriginal:boolean}[];
    /** trak the conversions using imagemagick */
    conversions:{name:string,from?:ObjectId,to?:ObjectId,date:number}[];
    /** build a story of the file in human readable format */
    story:string[];
    /** last time the file was updated */
    updated:number;
    /** true if a file is "valid", used for check if a file pass some controls */
    valid:boolean;
    /** trak all the controls the file has passed */
    validations:{date:number;valid:boolean}[];
    /** file is marked archived when it exit all workflow and normally is deleted by archiviation process */
    archived:number;
    /** if the file belongs to an HotFolder this is the shortcut to that HotFolder */
    HF:HotFolder;
    
    /** All MyFile istances */
    static files:MyFile[];
    /** db binding for files operations */
    static db:Db;

    /** return the name without extension */
    nameWOExt:string
    /** returns a promise of file informations */
    fileInfo:Promise<{exists:boolean,stats?:Stats}>

    /**
     * 
     * @param why Optional motivation of the deletion to add to the file story
     * 
     * Delete the file and mark it as deleted
     */
    delete(why:string):Promise<MyFile>

    /**
     * TODO
     * 
     * Copy the file to the specified destination folder
     */
    copyTo(isToDo:string):void

    /**
     * 
     * @param destFolder path of the destination folder
     * @param md if true makes the directory if it does not exists. Default: true
     * @param overwrite if true, if a file with same name exists on the destination folder, overwrite it. Default: true
     * @param emitEventForMoved optional: specifies if moved file hotfolder must emit deletion event. Default: true
     * @param emitEventForOverwritten optional: specifies if overwritten file hotfolder must emit deletion event. Default: false
     * 
     * Move the file to the specified destination folder.
     */
    moveTo(destFolder:string, md:boolean, overwrite:boolean, emitEventForMoved:boolean,emitEventForOverwritten:boolean):Promise<{file:MyFile,fail?:string}>
    /**
     * 
     * @param newPath path of the destination file
     * @param md if true makes the directory if it does not exists. Default: false
     * @param overwrite if true, if a file with same name exists on the destination folder, overwrite it. Default: true
     * @param emitEventForMoved optional: specifies if renameMoved file hotfolder must emit deletion event. Default: false
     * @param emitEventForOverwritten optional: specifies if overwritten file hotfolder must emit deletion event. Default: false
     * 
     * Rename and move the file to the specified destination folder
     */
    renameMove(newPath:string, md:boolean, overwrite:boolean, emitEventForMoved:boolean,emitEventForOverwritten:boolean):Promise<{file:MyFile,fail?:string}>

    /**
     * 
     * @param newName The new name of the file
     * 
     * Rename the file
     */
    rename(newName:string):Promise<{file:MyFile,fail?:string}>
    /**
     * 
     * @param otherFilePath the path to the file with which to replace this file
     * @param preserveOriginal if false, the file with which to replace this file will be deleted. Default: true
     * 
     * Replaces this file with another specified file.
     * If an hotfolder is binded to this file, add an exclusion on that hotfolder to avoid that
     * adding and deleting events brokes the workflow and hotfolder operations
     */
    replace(otherFilePath:string,preserveOriginal:boolean):Promise<{file:MyFile,fail?:string}>

    /**
     * Read the file and returns an array of lines
    */
    readLines():Promise<string[]>

    //TODO fix with tmp path all cases
    imConvert(name:string,settings:ImSetting[]):Promise<MyFile>
    imConvert(name:string,operators:ImOperator[]):Promise<MyFile>
    imConvert(name:string,settings:ImSetting[],operators:ImOperator[]):Promise<MyFile>
    imConvert(name:string,settings:ImSetting[],outputPath:string,preserveOriginal:boolean):Promise<MyFile>
    imConvert(name:string,operators:ImOperator[],outputPath:string,preserveOriginal:boolean):Promise<MyFile>
    imConvert(name:string,settings:ImSetting[],operators:ImOperator[],outputPath:string,preserveOriginal:boolean):Promise<MyFile>
    /**
     * 
     * @param name the nickname of the conversion
     * @param settings an array of settings to apply to the conversion
     * @param operators an array of operators to apply to the conversion
     * @param outputPath optional destination file path. If given, the next argument "preserveOriginal is mandatory"
     * @param preserveOriginal if false, the original file of the conversion will be deleted.
     * 
     * If this file is an image, uses imagemagick to convert it and do all what imagimagick can do.
     * This also work with pdf but in that case also need ghostscript installed on system.
     * N.B. If the outputPath is the same directory of the original file but with different file name, and preserveOriginal
     * is false, remember to use skipnext to avoid not wanted adding event of the HotFolder
     */
    imConvert(name:string,arg1:ImSetting[]|ImOperator[],arg2?:ImOperator[]|string,arg3?:boolean|string,arg4?:boolean):Promise<MyFile>
    /**
     * 
     * @param valid boolean
     * 
     * mark the file as valid or not valid
     */
    validate(valid:boolean):Promise<MyFile>
    
    /**
     * mark the file as archived
     */
    archive():Promise<MyFile>


    /**
     * 
     * @param path the path of a file
     * @param storyBegin optional string to push to the file story array
     * 
     * add a new MyFile istance for the given path. If the file already exists
     * on the database or is already opened it return that file instead
     */
    static add(path:string,storyBegin?:string):Promise<MyFile>
    /**
     * 
     * @param path the path of a file
     * @param story optional string to push to the file story array
     * 
     * only updates the file when hotfolder emit change events
     */
    static change(path:string,story?:string):Promise<MyFile>
    /**
     * 
     * @param path the path of a file
     * 
     * apply the delete method to files when hotfolder emit delete events
     */
    static delete(path:string):Promise<MyFile>
  }

}