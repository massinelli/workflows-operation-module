# Massinelli WorkFlows Operation Module

Modulo operazioni per Massinelli Workflow.
Template che permette di creare operazioni da applicare ai workflows del software Massinelli Workflows.

## Input e Output

L'input di ogni operazione è un osservabile [rxjs](https://rxjs-dev.firebaseapp.com/) che trasporta oggetti di tipo "MyFile".

L'output di ogni operazione deve essere un osservabile di oggetti MyFile e, per ogni oggetto MyFile che "entra" dall'osservabile di input deve uscire uno e solo uno oggetto MyFile (anche se diverso dall'input); se questa regola non viene rispettata il software Massinelli Workflow si bloccherà in errore.


## Oggetto MyFile

Le proprietà e i metodi di tale oggetto sono descritti di seguito:

### Proprietà:

__path__: il percorso completo del file

__name__: il nome file

__nameWOExt__: il nome file senza la sua estensione

__ext__: l'estensione (compresa del punto iniziale, ad esempio ".pdf")

__fileInfo__: ritorna un oggetto Dirent descrittivo del file

__size__: le dimensioni in byte

__insertDate__: la data in cui il file è rilevato dalla hotfolder (millisecondi formato UNIX)

__deleted__: la data in cui il file è stato cancellato (millisecondi formato UNIX)

__modifyDate__: la data dell'ultima modifica (millisecondi formato UNIX)

__birthDate__: la data di creazione (millisecondi formato UNIX)

__history__: un array di modifiche subite dal file. Ogni elemento dell'array ha la data del suo inserimento, il percorso del file, la sua data di modifica (se il file è stato fisicamente modificato) e la sua dimensione in byte.

__copies__: un array che tiene traccia delle copie del file, se presenti

__conversions__: un array che tiene traccia delle conversioni fatte con la funzione imConvert descritta più sotto

__story__: un array di stringhe, ogni stringa descrive in modo comprensibile ogni operazione fatta sul file

__updated__: l'ultima volta che l'istanza MyFile è stato aggiornata

__valid__: da usare se il processo prevede una validazione sui files

__validations__: un array che tiene traccia di tutte le validazioni subite dal file, passate e non

__archived__: se il workflow prevede un'archiviazione finale si può flaggare il file come archiviato

__HF__: collegamento all'interfaccia della HotFolder che contiene il file. Questa espone solo alcuni metodi che possono risultare utili durante la scrittura di un'operazione: skipNext(): utile per indicare alla hotfolder di non scatenare eventi durante la rilevazione di file nuovi, cancellati o modificati, in base al nome file o per un numero determinato di files, getFiles(): ritorna un array di files presenti nella Hotfolder e getTree(): simile a getFiles() ma ritorna un oggetto ad albero che rispecchia la struttura del filesystem all'interno della hotfolder


### Metodi:

__delete()__: elimina il file, l'istanza della classe MyFile rimane ma viene marchiata come "deleted"

__copyTo()__: copia il file

__moveTo()__: sposta il file

__renameMove()__: sposta il file e lo rinomina

__rename()__: rinomina il file

__replace()__: sostituisce il file con un altro

__readLines()__: se il file è un file di testo questo metodo ritorna un array di stringhe in cui ogni elemento è una riga del file

__imConvert()__: se il file è un file immagine questo metodo può applicare una conversione tramite il tool imageMagick (N.B. imagemagick deve essere preinstallato sul pc, se si desidera lavorare su files pdf è possibile ma richiede in aggiunta l'installazione di ghostscript)

__validate()__: marchia il file come valido

__archive()__: marchia il file come archiviato

__add()__: non esegue nulla sul file attuale ma aggiunge una nuova istanza di MyFile, accetta un percorso file come argomento e opzionalmente una stringa che descrive il motivo dell'aggiunta. La funzione ritorna una promise di un array di due elementi dove il primo elemento è il file su cui è stata chiamato il metodo e il secondo è il nuovo file


Tutte le proprietà e i metodi con i relativi parametri e tipi sono maggiormente descritti tramite JSDocs


## Installazione

Richiede [Node.js](https://nodejs.org/) v10+

Installa nodejs

clona il progetto:

```
$ git clone https://bitbucket.org/massinelli/workflows-operation-module.git
```

Installa le dipendenze:

```
$ cd workflows-operation-module
$ npm install
```

Usa il tuo IDE/code editor preferito per scrivere la tue operazioni editando "exampleOperation.ts" e salvando con diverso nome.
Il nome file scelto dovrà essere poi inserito senza estensione .ts nella configurazione dei workflows (workflows.json) del software Massinelli Workflows.