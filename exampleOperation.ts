import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'

const operationName = __filename.slice(0,__filename.lastIndexOf('.'))


module.exports = (in$:Observable<MyFile>):Observable<MyFile> => {

  const outputObservable = in$.pipe(
    tap( file => console.log(`a file (${file.name}) is passed in ${operationName} operation`) )
	)
	
  //This function MUST EVER return an Observable of MyFile
  return outputObservable

}