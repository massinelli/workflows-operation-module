import { Observable, interval } from 'rxjs'
import { tap, concatMap, take, finalize, groupBy, filter, map } from 'rxjs/operators'

const operationName = __filename.slice(0,__filename.lastIndexOf('.'))

module.exports = (in$:Observable<MyFile>):Observable<MyFile> => {

  const outputObservable = in$.pipe(
    tap( file => console.log(`a file (${file.name}) enters ${operationName} operation`) ),
    concatMap( file => interval(1000).pipe(
      tap( () => console.log(`this operation also do something more complex, it writes this line 10 times for each file`)),
      filter( x => x > 9 ),
      map( () => file ),
      take(1)
    )),
    tap( file => console.log(`a file (${file.name}) exit ${operationName} operation`) )
	)
	
  //This function MUST EVER return an Observable of MyFile
  return outputObservable

}